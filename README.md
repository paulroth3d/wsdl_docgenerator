#WSDL Documentation Generator

### Overview
Simply an Ant Script that executes the WSDL Viewer XSL file provided by T Vanek

### Current XSL file
[http://tomi.vanek.sk/xml/wsdl-viewer.xsl](http://tomi.vanek.sk/xml/wsdl-viewer.xsl)

### XSL Project Page
[http://tomi.vanek.sk/index.php?page=wsdl-viewer](http://tomi.vanek.sk/index.php?page=wsdl-viewer)

### Prerequisites
* Apache Ant - can be installed by multiple package managers (such as homebrew/macPorts/etc)

### To Run
1.	Place all WSDL files in the `source` directory
2.	Simply run the following command `ant apply` or simply `ant`
3.	Your files are generated in the `output` directory. 

### Example Result

Works with XSD and other method types and shows all methods by their Parameters and Responses each with hyperlinks.

![ScreenShot of Results](SampleOutputScreenshot.png)
